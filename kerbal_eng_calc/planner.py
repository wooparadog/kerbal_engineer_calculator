#!/usr/bin/env python

from kerbal_eng_calc import Combo, target, print_combo
from planets import get_planet, ORBIT_DV, ATMO

def debug(msg, node=None):
    print('[DEBUG] %s' % msg)
    if node is not None:
        print_combo(node.combo)

class Node(object):

    def __init__(self, combo, stage, dv, parent, children):
        self.combo = combo
        self.stage = stage
        self.children = children
        self.parent = parent
        self.dv = dv
        self.dead = False

def path_fitness(path):
    fit = 100.0
    payload_mass = path[0].combo.ship_mass
    ship_mass = path[-1].combo.ship_mass
    fit *= (4.0 / len(path))**2
    asp = 2.0
    for node in path:
        if node.combo.asparagus:
            asp += len(node.combo.engines)
    fit *= (4.0 / asp)
    fit *= float(payload_mass) / ship_mass
    return fit

def best_combos(mass, env, dv, atmo, stage):
    ''' Get up to 3 combos, higher priority over those that complete the dv requirement
    '''
    if atmo:
        min_twr = 2.0
    else:
        min_twr = 1.0
    if stage == 'lander':
        dv_mult = 5.0
        mass_mult = 2.0
        asp = False
    elif stage == 'interplanetary':
        dv_mult = 12.0
        mass_mult = 1.0
        asp = False
    elif stage == 'launch':
        dv_mult = 10.0
        mass_mult = 1.0
        asp = True
    else:
        raise ValueError('Unknown stage: %s' % stage)
    combos = target(mass, env, 
        atmo=atmo,
        dv_mult=dv_mult,
        mass_mult=mass_mult,
        min_twr=min_twr,
        asparagus=asp,
    )
    if not combos:
        return []
    return combos[:5]

def branch_out(node, env, dv, atmo, stage):
    combos = best_combos(node.combo.ship_mass, env, dv, atmo, stage)
    if not combos:
        #debug("Killing: %s" % str(node), node=node)
        node.dead = True
        parent = node.parent
        while parent and len(parent.children) == 1:
            parent.dead = True
            parent = parent.parent
        return None
    children = []
    for combo in combos:
        if node.stage == stage:
            children += [Node(combo, stage, node.dv + combo.dv, node, [])]
        else:
            children += [Node(combo, stage, combo.dv, node, [])]
    node.children = children
    return node

def debug_branch(stage, env, dv, root):
    print("*"*80)
    print("BRANCHING")
    print("Stage: %s\nEnv: %s" % (stage, env.name)) 
    print("Root Stage: %s\nDV to go: %s" % (root.stage, dv))

def grow_tree(root, env, dv, atmo, stage):
    ''' Expand on the leaves once '''
    if not root.children:
        if root.stage == stage and root.dv < dv:
            branch_out(root, env, dv - root.dv, atmo, stage)
        elif root.stage != stage:
            branch_out(root, env, dv, atmo, stage) 
    for node in root.children:
        grow_tree(node, env, dv, atmo, stage)
    return root

def calc_tree(mass, start_name, dest_name, start_orbit=False, dest_orbit=False, inter_dv=0.0):
    start = get_planet(start_name)
    dest = get_planet(dest_name)
    payload = Node(Combo(0, 0, 0, 0, mass, 0, [], [], False), 'lander', 0, None, [])
    tree = payload
    if start_orbit:
        start_dv = 0.0
    else:
        start_dv = start.orbit_dv
    if dest_orbit:
        dest_dv = 0.0
    else:
        dest_dv = dest.orbit_dv
    if dest_dv > 0.0:
        grow_tree(tree, dest, dest_dv, dest.atmo, 'lander')
    if inter_dv > 0.0:
        grow_tree(tree, start, inter_dv, False, 'interplanetary')
    if start_dv > 0.0:
        grow_tree(tree, start, start_dv, start.atmo, 'launch')
    return tree
         
def print_path(path):
    for node in path[1:]:
        print_combo(node.combo)

def print_paths(root):
    for path in build_paths(root, []):
        print_path(path)
        print('*'*80)

def build_paths(node, path):
    if node.dead:
        return
    path += [node]
    if not node.children:
        yield path
    for child in node.children:
        for new_path in build_paths(child, path[:]):
            yield new_path

def calc_path_fitnesses(root):
    paths = []
    for path in build_paths(root, []):
        fit = path_fitness(path)
        paths += [(fit, path)]
    paths.sort()
    paths.reverse()
    return paths

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('mass', type=int, help='Mass of payload')
    parser.add_argument('start', help='starting body')
    parser.add_argument('dest', help='ending body')
    parser.add_argument('--start-orbit', '-s', action='store_true', help='Starting in orbit')
    parser.add_argument('--dest-orbit', '-d', action='store_true', help='Ending in orbit')
    parser.add_argument('--inter-dv', '-i', type=int, default=0, help='interplanetary deltav required')
    args = parser.parse_args()
    tree = calc_tree(
        args.mass,
        args.start,
        args.dest,
        start_orbit=args.start_orbit,
        dest_orbit=args.dest_orbit,
        inter_dv=args.inter_dv
    )
    paths = calc_path_fitnesses(tree)
    for fit, path in paths:
        print('Fitness: %f' % fit)
        print_path(path)
        print('*'*80)
    
