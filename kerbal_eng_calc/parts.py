#!/usr/bin/env python
import json

NAME = 'name'
MOUNT = 'mount'
MASS = 'mass'
THRUST = 'thrust'
ATM_TS = 'atm t/s'
VAC_TS = 'vac t/s'
ATM_ISP = 'atm isp'
VAC_ISP = 'vac isp'
TVC = 'tvc'

WET_MASS = 'mass'
DRY_MASS = 'dry mass'
LIQ_FUEL = 'liquid fuel'
OXI_FUEL = 'oxidizer'
FUEL_MASS = 'fuel mass'

with open('parts.json') as f:
    JSON_PARTS = json.load(f)

class Part(object):
    
    def decode(self, dct):
        self.name = dct[NAME]
        self.mass = dct[MASS]

class Engine(Part):

    def decode(self, dct):
        Part.decode(self, dct)
        self.mount = dct[MOUNT]
        self.thrust = dct[THRUST]
        self.atm_ts = dct[ATM_TS]
        self.vac_ts = dct[VAC_TS]
        self.atm_isp = dct[ATM_ISP]
        self.vac_isp = dct[VAC_ISP]
        self.tvc = dct[TVC]

class Tank(Part):

    def decode(self, dct):
        Part.decode(self, dct)
        self.mount = dct[MOUNT]
        self.wet_mass = dct[WET_MASS]
        self.dry_mass = dct[DRY_MASS]
        self.liq_fuel = dct[LIQ_FUEL]
        self.oxi_fuel = dct[OXI_FUEL]
        self.fuel_mass = dct[FUEL_MASS]
    
ALL_PARTS = {}

ENGINES = {}
for engine, data in JSON_PARTS['engines'].items():
    ENGINES[engine] = Engine()
    ENGINES[engine].decode(data)
    ALL_PARTS[engine] = ENGINES[engine]

TANKS = {}
for tank, data in JSON_PARTS['tanks'].items():
    TANKS[tank] = Tank()
    TANKS[tank].decode(data)
    ALL_PARTS[tank] = TANKS[tank]
    
def get_engine(engine_name):
    return ENGINES[engine_name]

def get_tank(tank_name):
    return TANKS[tank_name]

'''
Reference
=========

Engines:
        'lv-1r': {NAME: 'LV-1R Liquid Fuel Engine'},
        '24-77': {NAME: 'Rockomax 24-77'},
        'mark55': {NAME: 'Rockomax Mark 55 Radial Mount Liquid Engine'},
        'lv-1': {NAME: 'LV-1 Liquid Fuel Engine'},
        '48-7s': {NAME: 'Rockomax 48-7S'},
        'lv-t30': {NAME: 'LV-T30 Liquid Fuel Engine'},
        'lv-t45': {NAME: 'LV-T45 Liquid Fuel Engine'},
        'lv-909': {NAME: 'LV-909 Liquid Fuel Engine'},
        'rapier': {NAME: 'R.A.P.I.E.R. Engine'},
        'aerospike': {NAME: 'Toroidal Aerospike Rocket'},
        'poodle': {NAME: 'Rockomax "Poodle" Liquid Engine'},
        'mainsail': {NAME: 'Rockomax "Mainsail" Liquid Engine'},
        'skipper': {NAME: 'Rockomax "Skipper" Liquid Engine'},
        'lv-n': {NAME: 'LV-N Atomic Rocket Motor'},
Tanks:
        'toroidal': {NAME: 'ROUND-8 Toroidal Fuel Tank'},
        'oscar-b': {NAME: 'Oscar-B Fuel Tank'},
        'fl-t100': {NAME: 'FL-T100 Fuel Tank'},
        'fl-t200': {NAME: 'FL-T200 Fuel Tank'},
        'fl-t400': {NAME: 'FL-T400 Fuel Tank'},
        'fl-t800': {NAME: 'FL-T800 Fuel Tank'},
        'x200-8': {NAME: 'Rockomax X200-8 Fuel Tank'},
        'x200-16': {NAME: 'Rockomax X200-16 Fuel Tank'},
        'x200-32': {NAME: 'Rockomax X200-32 Fuel Tank'},
        'x200-64': {NAME: 'Rockomax Jumbo-64 Fuel Tank'},
'''

