#!/usr/bin/env python

import sys
import math
from collections import namedtuple
from itertools import chain

from parts import ALL_PARTS, ENGINES, TANKS, get_engine, get_tank
from planets import PLANETS, get_planet, get_moon

Combo = namedtuple('Combo', 'fitness dv twr isp ship_mass secs engines tanks asparagus')

def total_thrust(engines):
    ''' Thrust of list of engines '''
    return sum([x.thrust for x in engines])

def total_mass(all_parts):
    ''' Mass of list of parts '''
    return sum([x.mass for x in all_parts])

def total_twr(mass, engines, planet):
    ''' calculate total twr given mass and a set of engines '''
    return calc_twr(
        total_mass(engines) + mass, 
        total_thrust(engines), 
        planet.grav
    )

def total_seconds(engines, tanks, atmo=False):
    ''' Calculate seconds of fuel '''
    isp = total_isp(engines, atmo=atmo)
    consumption_rate = total_thrust(engines) * 1000.0 / (isp * 9.82)
    consumption_rate /= 5000.0
    fuel = sum([x.fuel_mass for x in tanks])
    return fuel / consumption_rate / 5.0

def total_isp(engines, atmo=False):
    ''' total isp of engines '''
    if atmo:
        return (total_thrust(engines) / 
            sum([engine.thrust / engine.atm_isp for engine in engines]))
    else:
        return (total_thrust(engines) / 
            sum([engine.thrust / engine.vac_isp for engine in engines]))

def total_dv(mass, engines, tanks, atmo=False):
    ''' calculate deltav given a set of parts '''
    mass_start = mass + total_mass(engines + tanks)
    mass_dry = sum([x.dry_mass for x in tanks])
    mass_end = mass + total_mass(engines) + mass_dry
    isp = total_isp(engines, atmo=atmo)
    return math.log(mass_start / mass_end, math.e) * isp * 9.81

def asparagus_dv(mass, engine_tank_pairs, atmo=False):
    ''' calculate deltav of asparagus staged pairs.
    The first index will be dropped, then the next, and so on.
    '''
    dv = 0
    for i, engines_tanks in enumerate(engine_tank_pairs):
        engines, tanks = engines_tanks
        engines_stages = [pairs[0] for pairs in engine_tank_pairs[i:]]
        all_engines = []
        for stage in engines_stages:
            all_engines += stage
        dv += total_dv(mass, all_engines, tanks, atmo=atmo)
    return dv
    
def calc_twr(mass, thrust, gravity):
    ''' calculate twr given a few raw floats '''
    return thrust / (mass * gravity)

def get_names(all_parts):
    ''' names of all the parts '''
    names = ''
    name_d = {}
    for part in all_parts:
        if name_d.get(part, 0) == 0:
            name_d[part] = 1
        else:
            name_d[part] += 1
    for part, count in name_d.items():
        names += '%d x %s, ' % (count, part.name)
    return names[:-2]

def target_twr(twr, mass, planet):
    ''' Prints out the closest combo of engines for target twr '''
    calcs = []
    for engine_name in ENGINES:
        engine = ENGINES[engine_name]
        e_twr = total_twr(mass, [engine], planet)
        calcs += [(e_twr, [engine])]
        e_twr = total_twr(mass, [engine]*2, planet)
        calcs += [(e_twr, [engine]*2)]
        e_twr = total_twr(mass, [engine]*3, planet)
        calcs += [(e_twr, [engine]*3)]
        e_twr = total_twr(mass, [engine]*4, planet)
        calcs += [(e_twr, [engine]*4)]
        e_twr = total_twr(mass, [engine]*6, planet)
        calcs += [(e_twr, [engine]*6)]
        e_twr = total_twr(mass, [engine]*8, planet)
        calcs += [(e_twr, [engine]*8)]
    calcs.sort()
    i, closest = min(enumerate(calcs), key=lambda x: abs(x[1][0] - twr))
    print('Closest is %s with a twr of %f' % (
        get_names(closest[1]), closest[0]))
    print('More is %s with a twr of %f' % (
        get_names(calcs[i+1][1]), calcs[i+1][0]))
    print('Less is %s with a twr of %f' % (
        get_names(calcs[i-1][1]), calcs[i-1][0]))

def calc_fitness(dv, mass, ship_mass, twr=0.0, dv_mult=10.0, mass_mult=1.0,
    min_twr=1.0):
    if twr < min_twr:
        return 0.0
    fitness = 0.0
    fitness += dv * dv_mult
    fitness -= (ship_mass / mass) * dv * mass_mult
    return fitness

def calc_fitness_asparagus(dv, mass, ship_mass, twr=0.0, dv_mult=10.0, mass_mult=1.0,
    min_twr=1.0):
    if twr < min_twr:
        return 0.0
    fitness = 0.0
    fitness += dv * dv_mult
    fitness -= (ship_mass / mass) * dv * mass_mult
    return fitness

def print_combo(combo):
    if combo.asparagus:
        print('Staging: Asparagus')
    else:
        print('Staging: Standard')
    print('Fitness: %f' % combo.fitness)
    print('DeltaV : %f' % combo.dv)
    print('TWR    : %f' % combo.twr)
    if not combo.asparagus:
        print('Seconds: %f' % combo.secs)
    print('Mass   : %f' % combo.ship_mass)
    if combo.asparagus:
        print('%d stages of' % len(combo.engines))
        print('\tEngines: %s' % get_names(combo.engines[0]))
        print('\tTanks  : %s\n' % get_names(combo.tanks[1]))
    else:
        print('Engines: %s' % get_names(combo.engines))
        print('Tanks  : %s\n' % get_names(combo.tanks))

def proper_num_engines(tank, engine, tanks_n):
    if tank.mount == 'Tiny':
        valid = {
            'Radial mounted': {
                1: [1, 2],
                2: [2, 4],
                4: [4, 8],
                8: [8],
                16: [16],
            },
            'Tiny': {
                1: [1],
                2: [2],
                4: [4],
                8: [8],
                16: [16],
            },
        }
    elif tank.mount == 'Small':
        valid = {
            'Radial mounted': {
                1: [1, 2, 4, 8],
                2: [2, 4, 8],
                4: [4, 8],
                8: [8],
                16: [16],
            },
            'Tiny': {
                1: [1, 2, 4, 8],
                2: [2, 4, 8],
                4: [4, 8],
                8: [8, 16],
                16: [16],
            },
            'Small': {
                1: [1],
                2: [2],
                4: [4],
                8: [8],
                16: [16],
            },
        }
    elif tank.mount == 'Large':
        valid = {
            'Radial mounted': {
                1: [1, 2, 4, 8],
                2: [2, 4, 8],
                4: [4, 8],
                8: [8, 16],
                16: [16, 32],
            },
            'Tiny': {
                1: [2, 4, 8],
                2: [4, 8],
                4: [8, 16],
                8: [16],
                16: [32],
            },
            'Small': {
                1: [1, 2, 4],
                2: [2, 4, 6, 8],
                4: [4, 8, 12],
                8: [8, 16],
                16: [16, 32]
            },
            'Large': {
                1: [1],
                2: [2],
                4: [4],
                8: [8],
                16: [16],
            },
        }
    else:
        raise ValueError('Mount type: %s' % engine.mount)
    if engine.mount not in valid:
        return []
    if tanks_n not in valid[engine.mount]:
        return []
    return valid[engine.mount][tanks_n]

def tank_engine_combo():
    for tank_name, tank in TANKS.items():
        for tanks_n in [1,2,4,8,16]:
            tanks = [tank] * tanks_n
            for engine_name, engine in ENGINES.items():
                for n in proper_num_engines(tank, engine, tanks_n):
                    engines = [engine] * n
                    yield engines, tanks

def asparagus_combo():
    for stages in [2,3,4]:
        for tank_name, tank in TANKS.items():
            for tanks_n in [2,4]:
                tanks = [tank] * tanks_n
                for engine_name, engine in ENGINES.items():
                    for n in proper_num_engines(tank, engine, tanks_n):
                        engines = [engine] * n
                        pairs = []
                        for stage in range(stages):
                            pairs += [(engines, tanks)]
                        yield pairs
                        


def target(mass, planet, atmo=False, asparagus=True, min_twr=1.0, mass_mult=1.0,
    dv_mult=10.0):
    ''' Find the best tank/engine combo to get a good deltav and TWR '''
    combos = []
    for engines, tanks in tank_engine_combo():
        tanks_mass = sum([x.mass for x in tanks])
        twr = total_twr(mass + tanks_mass, engines, planet)
        if twr < 1.0 or (atmo and twr < 2.0):
            continue
        secs = total_seconds(engines, tanks, atmo=atmo)
        isp = total_isp(engines, atmo=atmo)
        dv = total_dv(mass, engines, tanks, atmo=atmo)
        ship_mass = total_mass(engines + tanks) + mass
        fit = calc_fitness(dv, mass, ship_mass, twr=twr, min_twr=min_twr,
            mass_mult=mass_mult, dv_mult=dv_mult)
        combos += [
            Combo(
                fit, dv, twr, isp, ship_mass, secs, 
                engines, tanks, False
            )
        ]
    for engines_tanks_pairs in asparagus_combo():
        if not asparagus:
            break
        engine_sets, tank_sets = zip(*engines_tanks_pairs)
        engines = list(chain(*engine_sets))
        tanks = list(chain(*tank_sets))
        tanks_mass = sum([x.mass for x in tanks])
        twr = total_twr(mass + tanks_mass, engines, planet)
        if twr < 1.0 or (atmo and twr < 2.0):
            continue
        secs = -1
        dv = asparagus_dv(mass, engines_tanks_pairs, atmo=atmo)
        ship_mass = total_mass(engines + tanks) + mass
        fit = calc_fitness_asparagus(dv, mass, ship_mass, twr=twr, 
            min_twr=min_twr, mass_mult=mass_mult, dv_mult=dv_mult)
        combos += [
            Combo(
                fit, dv, twr, isp, ship_mass, secs,
                engine_sets, tank_sets, True
            )
        ]
    combos = sorted(combos)
    combos.reverse()
    return combos

for engine_name in ENGINES:
    engine_name_attr = engine_name.replace('-', '_')
    setattr(sys.modules[__name__], engine_name_attr, get_engine(engine_name))

for tank_name in TANKS:
    tank_name_attr = tank_name.replace('-', '_')
    setattr(sys.modules[__name__], tank_name_attr, get_tank(tank_name))

for planet_name in PLANETS:
    planet_name_attr = planet_name.replace('-', '_')
    setattr(sys.modules[__name__], planet_name_attr, get_planet(planet_name))

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('mass', type=float)
    parser.add_argument('planet')
    parser.add_argument('--atmosphere', '-a', action='store_true')
    parser.add_argument('--no-asparagus', '-n', action='store_true')
    parser.add_argument('--mass-mult', '-m', type=float, default=1.0)
    parser.add_argument('--dv-mult', '-d', type=float, default=10.0)
    parser.add_argument('--min-twr', '-t', type=float, default=1.0)
    parser.add_argument('--start', '-s', type=int, default=0)
    parser.add_argument('--length', '-l', type=int, default=1)
    args = parser.parse_args()
    combos = target(args.mass, get_planet(args.planet),
        atmo=args.atmosphere,
        asparagus=not args.no_asparagus,
        dv_mult=args.dv_mult,
        mass_mult=args.mass_mult,
        min_twr=args.min_twr,
    )
    args.length = args.length if args.length != 0 else len(combos)
    for i in range(args.length):
        try:
            print_combo(combos[i+args.start])
        except IndexError:
            break
