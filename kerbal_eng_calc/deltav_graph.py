#!/usr/bin/env python
from planets import get_planet

class DVNode(object):
    
    __DIRECT = {}

    def __init__(self, name,
        high_orbit=None, low_orbit=None, txfr=False, planet=None,
        **neighbors_kwargs):
        self.name = name
        self.neighbors = {}
        for node, cost in neighbors_kwargs.items():
            if isinstance(cost, tuple):
                self.neighbors[node] = cost[0]
                self.get(node).add(cost[1], self.name)
            else:
                self.neighbors[node] = cost
                self.get(node).add(cost, self.name)
        DVNode.__DIRECT[name] = self

    def add(self, cost, node):
        self.neighbors[node] = cost

    @classmethod
    def get(cls, name):
        return cls.__DIRECT[name]
    
    def get_path(self, origin, target, path):
        path += [(self, self.get(origin))]

dv_kerbin = DVNode('kerbin', planet=get_planet('kerbin'))
dv_kerbin_lo = DVNode('kerbin_lo', low_orbit=dv_kerbin,
    kerbin=(0, 4500))
dv_keo_txfr = DVNode('keo_txfr', txfr=True,
    kerbin_lo=(0, 680))
dv_keo = DVNode('keo', high_orbit=dv_kerbin,
    keo_txfr=435)
dv_mun_txfr = DVNode('mun_txfr', txfr=True,
    keo_txfr=(0, 180))
dv_mun = DVNode('mun', planet=get_planet('mun'))
dv_mun_lo = DVNode('mun_lo', low_orbit=dv_mun,
    mun=580)
dv_mun_esc = DVNode('mun_esc', high_orbit=dv_mun,
    mun_lo=230, mun_txfr=80)
dv_minmus = DVNode('minmus', planet=get_planet('minmus'))
dv_minmus_lo = DVNode('minmus_lo', low_orbit=dv_minmus,
    minmus=180)
dv_minmus_esc = DVNode('minmus_esc', high_orbit=dv_minmus,
    minmus_lo=70)
dv_minmus_txfr = DVNode('minmus_txfr', txfr=True,
    mun_txfr=(0,70))
dv_kerbin_esc = DVNode('kerbin_esc', high_orbit=dv_kerbin,
    minmus_txfr=(0,20))
dv_eve = DVNode('eve', planet=get_planet('eve'))
dv_eve_lo = DVNode('eve_lo', low_orbit=dv_eve,
    eve=(0,12000))
dv_gilly = DVNode('gilly', planet=get_planet('gilly'))
dv_gilly_lo = DVNode('gilly_lo', low_orbit=dv_gilly,
    gilly=30)
dv_gilly_esc = DVNode('gilly_esc', high_orbit=dv_gilly,
    gilly_lo=10)
dv_gilly_txfr = DVNode('gilly_txfr', txfr=True,
    gilly_esc=450, eve_lo=(0,1270))
dv_eve_esc = DVNode('eve_esc', high_orbit=dv_eve,
    gilly_txfr=(0,80))
dv_eve_txfr = DVNode('eve_txfr', txfr=True,
    eve_esc=(0,80), kerbin_esc=(0,90))
dv_duna = DVNode('duna', planet=get_planet('duna'))
dv_duna_lo = DVNode('duna_lo', low_orbit=dv_duna,
    duna=(0,1300))
dv_ike = DVNode('ike', planet=get_planet('ike'))
dv_ike_lo = DVNode('ike_lo', low_orbit=dv_ike,
    ike=390)
dv_ike_esc = DVNode('ike_esc', high_orbit=dv_ike,
    ike_lo=150)
dv_ike_txfr = DVNode('ike_txfr', txfr=True,
    duna_lo=(0,330), ike_esc=30)
dv_duna_esc = DVNode('duna_esc', high_orbit=dv_duna,
    ike_txfr=(0,30))
dv_duna_txfr = DVNode('duna_txfr', txfr=True,
    duna_esc=(0,250), kerbin_esc=(0,140))


