Kerbal Engineer Calculator
==========================

Helps determine parts needed for target TWR with best specificity.

        TWR = Thrust / (mass * gravity)
        Isp = Sum(Thrust foreach engine) / Sum(Thrust/Isp foreach engine)
        dV = ln(MassStart / MassEnd) * Isp * gravity

Sources:
http://wiki.kerbalspaceprogram.com/wiki/Cheat_Sheet
